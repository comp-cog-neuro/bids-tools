% Sample script for converting OHBA EEG to BIDS format
% Requires:
% EEGLAB: https://sccn.ucsd.edu/eeglab/index.php
% bids-matlab-tools plugin: https://github.com/sccn/bids-matlab-tools
% loadcurry plugin: https://github.com/mattpontifex/loadcurry
% Based on bids_export_example.m by A. Delorme

%% Load each curry file and save as a .set
curryFiles = {'.\source\MMN_combined_Oct_16.cdt', '.\source\MMN_combined_Oct_22.cdt'};
for iFile = 1:length(curryFiles)
    thisFile = curryFiles{iFile};
    EEG = loadcurry(thisFile);
    pop_saveset(EEG,thisFile);
end

%% BIDS conversion

% data files and session/run membership
% add more participants as needed, e.g. data(2).file = ...
data(1).file = {'./source/MMN_combined_Oct_16.set','.\source\MMN_combined_Oct_22.set'};
data(1).session = [1, 2];
data(1).run = [1, 1];

% general info
generalInfo.Name = 'MMN Combined';
generalInfo.ReferencesAndLinks = { "https://www.huntlab.co.uk/", "https://git.fmrib.ox.ac.uk/comp-cog-neuro" };

% event info
eInfoDesc.onset.Description = 'Event onset';
eInfoDesc.onset.Units = 'second';
eInfoDesc.value.Description = 'Value of event (numerical)';

% task info
tInfo.InstitutionAddress = 'Warneford Hospital, Warneford Ln, Headington, Oxford OX3 7JX';
tInfo.InstitutionName = 'University of Oxford';
tInfo.InstitutionalDepartmentName = 'Department of Psychiatry';
tInfo.PowerLineFrequency = 50;
tInfo.ManufacturersModelName = 'Synamps';

% Possible to add:
% - trialTypes (gives names to trigger values)
% - channel locations file (if different from what loadcurry identifies 
% - code (actual task code)
% - stimuli (and images/sounds in task)
% - a README (can be added here, or manually later)
% - a CHANGES file (for tracking changes to dataset)
% - participant info such as handedness
% - add meaning of events to eInfoDesc

% export to bids
bids_export(data, 'targetdir', './raw', 'taskName', 'mmn-combined', 'gInfo', generalInfo, 'eInfoDesc', eInfoDesc, 'tInfo', tInfo);
